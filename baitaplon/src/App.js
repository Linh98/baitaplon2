import React, { Component } from 'react'
import './App.css';
import {  BrowserRouter as Router, Route, Link, Prompt, Redirect} from 'react-router-dom'


const  Home = () =>(
  <div id="header">
       <h2>Trang Chủ</h2>
	   
	   <img src="template/banner.jpg" width="900" height="200" alt="Quản Lý Thiết Bị"/>
	 
    
 
  <div id="footer">
      <h3>©2020</h3>
  </div>
  </div>
)


const About = () => (
  <div id="header">
    <h2>Đây là trang Web báo cáo trạng thái của phòng máy</h2>
	 <img src="template/banner.jpg" width="900" height="200" alt="Quản Lý Thiết Bị"/>
	 <div id="footer">
      <h3>©2020</h3>
  </div>
  </div>
)

const Topic = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
)

const Topics = ({ match }) => (
   
     
  <div>
   <Prompt   when={1}   message={location => `Bạn Có Chắc Là Muốn Tới? ${location.pathname}` }/>
    <div id="header">
	 <h1> Quản Lý Thiết Bị </h1>

     
	 	  
     <div id="header">
	   
	   <img src="template/banner.jpg" width="900" height="200" alt="Quản Lý Thiết Bị"/>
	 </div>
	
	 
     <div id="Welcome">
        <div className="panitem" id="left" />
        <div className="panitem" id="content">
		  <fieldset>
		     <legend> Form Cập Nhật Trạng Thái Thiết Bị</legend>
			 <form action="/a" >
			    <table>
				 <tbody><tr>
				    <td width="100px">Username</td>
					<td><input type="text" name="txtUser"  /></td>
					</tr>
				 
                   <tr>
                    <td>Tình trạng</td>
					  <input type="radio"  name="rdoTinhtrang" defaultValue={0}/> Tốt
                    
                      <input type="radio"  name="rdoTinhtrang" defaultValue={0}/> Hỏng
                      
				   </tr>
				   <tr>
				    <td> Chọn Thiết Bị</td>
					<td>
					   <select name="sltThietbi">
					     <option value="mh">Màn Hình</option>
						 <option value="vmt">Vỏ Máy Tính</option>
						 <option value="oc">Ổ Cứng</option>
						 <option value="od">Ổ Đĩa</option>
						 <option value="c">Chuột</option>
						 <option value="bp">Bàn Phím</option>
					   </select>
                    </td>
                   </tr>					
                   <tr>	
                    <td>Note</td>
                    <td>
                       <textarea name="txtNote" cols={100} rows={10} defaultValue={""} />
                    </td>
				   </tr>
                   <tr>
                    <td colSpan={2} align="center">
					
                      <input type="Submit"  name="btnSubmit" defaultValue="Gửi Form"  />
                     
                    </td>
				   </tr>	
                  </tbody>
				 </table>				 
			 </form>
          </fieldset>
    </div>
	    <div className="panitem" id="right" />
	</div>
	<div className="clear" />
	<div id="footer">
	    <div id="footer" className="font"> ©2020</div>
      
    </div>
    <div className="clear" />
    <div id="footer" className="font"></div>
    </div> 
    
  </div>
)


class App extends Component {
  render() {
    return (

      <Router>
	    <div id="section">
      
          <ul>
            <li><Link to="/">Trang Chủ</Link></li>
            <li><Link to="/about">Giới Thiệu</Link></li>
            <li><Link to="/topics">Báo Cáo</Link></li>
			
          </ul>

          <hr/>

          <Route exact path="/" component={Home}/>
          <Route path="/about" component={About}/>
          <Route path="/topics" component={Topics}/>
		  
        </div>
		
      </Router>
    )
  }
}

export default App